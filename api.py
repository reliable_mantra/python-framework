from parse import parse
from webob import Request, Response


class API:
    def __init__(self):
        """Initialize API object

        Example routes:
            {
                '/home': <function home at 0x108d66730>
            }
        """
        self.routes = {}

    def __call__(self, environ, start_response):
        """Function call operator implementation: API(), will get called by WSGI on each request
        
        Arguments:
            environ {dict} -- WSGI prepared dictionary containing all envirinment variables
            start_response {func} -- Function that attacthes response status and headers
        
        Returns:
            response() result -- List containing bytes object as response
        """
        request = Request(environ)

        response = self.handle_request(request)

        #  According to PEP 333, the document which specifies the details of WSGI,
        #  our app interface has to return the response body as strings in an iterable,
        #  and that is what webob response __call__ will give us:
        #
        #  def __call__(self, environ, start_response):
        #       headerlist = self._abs_headerlist(environ)
        #       start_response(self.status, headerlist)
        #       return self._app_iter
        response_body = response(environ, start_response)

        return response_body

    def route(self, path):
        """Handle the route decorator
        
        Arguments:
            path {str} -- The route path with params
        
        Raises:
            AssertionError: Raises an exception for duplicate routes

        Returns:
            wrapper -- Function that will store and return the appropriate route handler
        """
        if path in self.routes:
            raise AssertionError("Such route already exists.")

        #  The returned wrapper will receive the handler, store it and then return it
        def wrapper(handler):
            self.routes[path] = handler
            return handler

        return wrapper

    def default_response(self, response):
        """Return a default response when a route is missing
        
        Arguments:
            response -- webob response object
            
        Returns:
            response -- Prepared default webob response object
        """
        response.status_code = 404
        response.text = "Not found."

    def find_handler(self, request_path):
        """Find the route handler
        
        Arguments:
            request_path {str} -- Tre route
        
        Returns:
            handler -- The route handler
            params -- Dictiionary of route parameters
        """
        for path, handler in self.routes.items():
            #  Parse named route parameters such as "/hello/{name}"
            parse_result = parse(path, request_path)
            if parse_result is not None:
                return handler, parse_result.named

        return None, None

    def handle_request(self, request):
        """Handle the webob wrapped WSGI request
        
        Arguments:
            request {webob request object} -- WSGI environ wrapped in webob request object
        
        Returns:
            response -- Handled webob response object
        """
        response = Response()

        handler, kwargs = self.find_handler(request_path=request.path)

        if handler is not None:
            #  Every route handler will get the request and response objects,
            #  plus any route parameters, as arguments
            handler(request, response, **kwargs)
        else:
            self.default_response(response)

        return response
